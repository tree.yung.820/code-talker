import env from "./util/env";

// Update with your config settings.

const defaults = {
  debug: false,
  client: "pg",
  connection: {
    database: env.DB_NAME || "code_talker",
    user: env.DB_USERNAME || "postgres",
    password: env.DB_PASSWORD || "postgres",
  },
  pool: {
    min: 2,
    max: 10,
  },
  migrations: {
    tableName: "knex_migrations",
  },
};

const knexConfigs = {
  development: {
    ...defaults,
    // debug: true,
  },

  staging: {
    ...defaults,
  },

  production: {
    ...defaults,
  },
  test: {
    client: "pg",
    connection: {
      host: process.env.POSTGRES_HOST,
      database: process.env.POSTGRES_DB,
      user: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "knex_migrations",
    },
  },
};

export default knexConfigs;

