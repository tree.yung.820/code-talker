import { EmailService } from "../services/email_service";

let emailService: EmailService;
export function getEmailService() {
    if (!emailService) {
        emailService = new EmailService();
    }
    return emailService;
}
