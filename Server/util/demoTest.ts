import { exec } from "child_process";
import path from "path";

function promiseSubmit(testFilePath: string) {
	return new Promise<any>((resolve, reject) => {
		exec(`yarn jest ${testFilePath}`, (error: any, stdout: any, stderr: any) => {
			if (error) {
				console.log(`error: ${error.message}`);
				console.log(`error`, error.message);
				resolve({ status: "fail", result: error.message });
				return;
			}
			if (stderr) {
				// console.log(`stderr: ${stderr}`);
				// console.log(`stderr`, stderr);
				resolve({ status: "pass", result: stderr });
				//all success
				return;
			}
			// console.log(`stdout: ${stdout}`);
			console.log(`stdout`);
			resolve({ result: stdout });
			return;
		});
	});
}

function promiseTest(testFilePath: string) {
	return new Promise<any>((resolve, reject) => {
		exec(`npx ts-node ${testFilePath}`, (error: any, stdout: any, stderr: any) => {
			if (error) {
				console.log(`error: ${error.message}`);
				console.log(`error`, error.message);
				resolve({ status: "fail", result: error.message });
				return;
			}
			if (stderr) {
				console.log(`stderr: ${stderr}`);
				console.log(`stderr`, stderr);
				resolve({ status: "pass", result: stderr });
				return;
			}
			console.log(`stdout: ${stdout}`);
			console.log(`stdout`, stdout);
			resolve({ status: "pass", result: stdout });
			return;
		});
	});
}

function execTest(testFilePath: string) {
	exec(`yarn jest ${testFilePath}/index`, (error: any, stdout: any, stderr: any) => {
		if (error) {
			console.log(`error: ${error.message}`);
			return error.message;
		}
		if (stderr) {
			console.log(`stderr: ${stderr}`);
			return stderr;
		}
		console.log(`stdout: ${stdout}`);
		return stdout;
	});
}
//for submit
export async function submitCode(roomId: string) {
	// let testFilePath = path.join(`${roomId}`);
	// console.log("submitFilePath :", testFilePath);
	return await promiseSubmit(roomId);
}
//for test / execute
export async function testCode(roomId: string) {
	let testFilePath = path.join(`code-room/${roomId}/index.ts`);
	console.log("testFilePath :", testFilePath);
	// return await promiseTest(testFilePath);
	return await promiseTest(testFilePath);
}

// submitCode("js", 1);
