export class HttpError extends Error {
    constructor(public status: number, public message: string) {
        super(message);
    }
}

export type User = {
    id: number;
    username: string;
    email: string;
    password?: string;
    avatar: string;
    role: string;
    status: string;
};

export type GoogleInfo = {
    id: string;
    email: string;
    verified_email: boolean;
    name: string;
    given_name: string;
    family_name: string;
    picture: string;
    LocaleEnum: string;
};

export type UserField = {
    username?: string;
    password: string;
    confirmPassword: string;
};

export type Email = {
    id: number;
    from: string;
    to: string;
    title: string;
    content: string;
    status: string;
};

declare global {
    namespace Express {
        interface Request {
            user?: {
                user_id: number;
                role: string;
                username: string;
                email: string;
                status: string;
            };
        }
    }
}
