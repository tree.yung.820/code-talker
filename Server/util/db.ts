import Knex from "knex";
import env from "./env";
import knexConfig from "../knexfile";

export const knex = Knex(knexConfig[env.NODE_ENV || "development"]);
