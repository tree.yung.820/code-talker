export function setRedis(redisClient: any) {
	redisClient.on("error", console.error);
	redisClient
		.connect()
		.then(() => console.log("Connected to redis locally!"))
		.catch(() => {
			console.error("Error connecting to redis");
		});
}
