import { config } from "dotenv";

config();

const env = {
    DB_NAME: process.env.DB_NAME || "code_talker",
    DB_USERNAME: process.env.DB_USERNAME || "postgres",
    DB_PASSWORD: process.env.DB_PASSWORD || "postgres",
    POSTGRES_DB: process.env.POSTGRES_DB_NAME || "code_talker_test",
    POSTGRES_USER: process.env.POSTGRES_USER || "postgres",
    POSTGRES_PASSWORD: process.env.POSTGRES_PASSWORD || "postgres",
    POSTGRES_HOST: process.env.POSTGRES_HOST || "postgres",
    GOOGLE_CLIENT_ID: process.env.GOOGLE_CLIENT_ID,
    GOOGLE_CLIENT_SECRET: process.env.GOOGLE_CLIENT_SECRET,
    PORT: process.env.PORT || 8080,
    SECRET: process.env.SECRET || "code_talker-secret",
    NODE_ENV: process.env.NODE_ENV || "development",
    WINSTON_LEVEL: process.env.NODE_ENV == "development" ? "debug" : "info",
    GMAIL_ACCOUNT: process.env.NODE_ENV || "codetalker237@gmail.com",
    GMAIL_APP_PASSWORD: process.env.NODE_ENV || "vwdptsybimujudop",
    CORS_IP: process.env.CORS_IP || "localhost:8080",
    EMAIL_CLIENT_USER: process.env.EMAIL_CLIENT_USER,
    EMAIL_CLIENT_PASSWORD: process.env.EMAIL_CLIENT_PASSWORD,
    EMAIL_SERVICE_PROVIDER: process.env.EMAIL_SERVICE_PROVIDER,
    EMAIL_HOST: process.env.EMAIL_HOST,
    EMAIL_CLIENT_FROM: process.env.EMAIL_CLIENT_FROM,
};

export default env;
