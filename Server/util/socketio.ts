import * as fs from "fs";
import { knex } from "./db";
import path from "path";
import { submitCode, testCode } from "./demoTest";
import { camelCaseKeys } from "./helper";
import { RoomService } from "../services/room_service";
import { q1, q2 } from "./demoInitString";

export function setSocketIO(io: any, redisClient: any) {
	// const roomName = "room1";
	const RS = new RoomService(knex);
	io.on("connection", (socket: any) => {
		let roomId: string;
		let timer = false;
		socket.on("CONNECTED_TO_ROOM", async (roomMeta: any) => {
			console.log("connect_to_room triggered");
			console.log("connect_to_room roomId: ", roomMeta);
			console.log("connect_to_room socketId: ", roomMeta.socketId);
			roomId = roomMeta.roomId;
			socket.join(roomMeta.roomId);
			// const markdown = fs.readFileSync(path.join(`markdown/${roomMeta.question_md}`));
			if (roomMeta.questionId == 1) {
				await redisClient.hSet(roomId, "content", q1);
			} else if (roomMeta.questionId == 2) {
				await redisClient.hSet(roomId, "content", q2);
			}

			const codeOnRedis = await redisClient.hGet(roomId, "content");
			io.to(roomId).emit("code_current", { code: codeOnRedis, id: roomMeta.socketId });
			socket.broadcast.emit("member_join", { socketId: roomMeta.socketId.toString(), userId: roomMeta.userId });
		});

		socket.on("CODE_CHANGED", async (data: any) => {
			console.log("code change: ", data);

			const fetchTimer = setTimeout(async () => {
				await redisClient.hSet(data.roomId, "content", data.code);
				console.log("fetched");
				timer = false;
			}, 2000);

			const fetchToRedis = () => {
				if (!timer) {
					timer = true;
					fetchTimer;
				} else {
					clearTimeout(fetchTimer);
					timer = true;
					fetchTimer;
				}
			};
			fetchToRedis();
			socket.broadcast.to(data.roomId).emit("code_current", { code: data.code, id: socket.id });
		});
		socket.on("execute", async (data: any) => {
			console.log("execute socket trigger");
			const { roomId, question_id, language } = data;
			const writeData = (await redisClient.hGet(roomId, "content")) as string;
			fs.writeFileSync(path.join(`code-room/${roomId}/index.${language}`), writeData, {
				flag: "w",
			});
			console.log("file wrote");
			const testRes = await testCode(`${roomId}`);
			socket.broadcast.emit("test_case", testRes);
			socket.emit("test_case", testRes);
		});
		socket.on("submit", async (data: any) => {
			console.log("submit socket trigger");
			const { code, roomId, question_id, userId } = data;
			// console.log(data);

			await redisClient.hSet(roomId, "content", code);
			const writeData = ("export " + (await redisClient.hGet(roomId, "content"))) as string;
			//write test.ts
			fs.writeFileSync(
				path.join(`code-room/${roomId}/test/index.test.ts`),
				fs.readFileSync(path.join(`solution/${question_id}/index.test.ts`)),
				{ flag: "w" }
			);
			//write user input
			fs.writeFileSync(path.join(`code-room/${roomId}/index.ts`), writeData, {
				flag: "w",
			});
			console.log("file wrote");
			const testRes = await submitCode(`${roomId}`);
			console.log("testRes: ", testRes);
			socket.broadcast.emit("result", testRes);
			socket.emit("result", testRes);
			if (testRes.status === "pass") {
				for (let id of userId) {
					if (id === null) {
						return;
					}
					await RS.insertUserCompletionHistory(id, question_id, "completed");
				}
			}
		});
		socket.on("disconnect", async () => {
			// if (io.sockets.adapter.rooms.get(roomId) === undefined) {
			// 	setTimeout(async () => {
			// 		if (io.sockets.adapter.rooms.get(roomId) === undefined) {
			// 			await redisClient.hDel(roomId, "content");
			// 			fs.rmSync(path.join(`code-room/${roomId}`), { recursive: true, force: true });
			// 			console.log("redis delete");
			// 		}
			// 	}, 10000);
			// }
			// if (io.sockets.adapter.rooms.get(roomId) === undefined) {
			// 	setTimeout(async () => {
			// 		await redisClient.hDel(roomId, "content");
			// 		console.log("redis delete");
			// 	}, 5000);
			// // }
			// try {
			// 	await redisClient.hDel(roomId, "content");
			// 	fs.rmSync(path.join(`code-room/${roomId}`), { recursive: true, force: true });
			// 	console.log("redis delete");
			// 	console.log("user disconnect");
			// } catch (error) {
			// 	console.error(error);
			// }
		});
	});
}
