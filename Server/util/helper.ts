import { HttpError } from "./models";
import camelcaseKeys from "camelcase-keys";

export function validate(obj: {}) {
	for (let key in obj) {
		if (!obj[key] || typeof obj[key] !== "string") {
			throw new HttpError(400, `Missing ${key}`);
		}
	}
}
export function camelCaseKeys(obj: any) {
	return camelcaseKeys(obj);
}
