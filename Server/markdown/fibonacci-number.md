The Fibonacci numbers, commonly denoted `F(n)` form a sequence, called the Fibonacci sequence, such that each number is the sum of the two preceding ones, starting from 0 and 1. That is,\
\
`F(0) = 0, F(1) = 1`\
`F(n) = F(n - 1) + F(n - 2), for n > 1.`\
Given n, calculate `F(n).`\
\
Your Function should handle situations where it would throw errors when `n` is:
`Number.POSITIVE_INFINITY`\
`Number.NEGATIVE_INFINITY`\
`Float Number e.g. 5.6`\
`> 200`
\
\
**Example 1:**

`Input: n = 2`\
`Output: 1`\
\
**Example 2:**

`Input: n = 10`\
`Output: 55`\
\

**Example 3:**

`Input: n = 201`\
`Output: Error`\

**Example 4:**

`Input: n = 5.6`\
`Output: Error`\

**Example 5:**

`Input: n = 5.6`\
`Output: Error`\

**Example 6:**
`Input: n = Number.POSITIVE_INFINITY || Number.NEGATIVE_INFINITY`\
`Output: Error`\
