import express from "express";
import cors from "cors";
import http from "http";
import { Server as SocketIO } from "socket.io";
import { createClient } from "redis";
import { oAuthMiddleWare, sessionMiddleWare } from "./middleware/middleware";
import { setSocketIO } from "./util/socketio";
import { attachApi } from "./api";
import env from "./util/env";
import { setRedis } from "./util/redis_client";

//express server
const app = express();
const PORT = env.PORT;

//redis
const redisClient = createClient();
setRedis(redisClient);

//socketIO
const server = new http.Server(app);
const io = new SocketIO(server, {
	cors: {
		//need to change to your own ip:8080 first in .env
		origin: env.CORS_IP,
		methods: ["GET", "POST"],
	},
});
setSocketIO(io, redisClient);

app.use(cors());
app.use(express.json({ limit: "10mb" }));
app.use(sessionMiddleWare);
app.use(oAuthMiddleWare);

app.use(express.static("public"));
app.use(express.static("uploads"));

attachApi(app, io);

server.listen(PORT, () => {
	console.log(`Listening at port http://localhost:${PORT}`);
});
