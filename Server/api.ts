import { Express } from "express";
import { knex } from "./util/db";
import { UserController } from "./controllers/user_controller";
import { UserService } from "./services/user_service";
import SocketIO from "socket.io";
import { QuestionController } from "./controllers/question_controller";
import { QuestionService } from "./services/question.service";
import { RoomController } from "./controllers/room_controller";
import { RoomService } from "./services/room_service";

export function attachApi(app: Express, io: SocketIO.Server) {
    let userService = new UserService(knex);
    let userController = new UserController(userService);
    let questionService = new QuestionService(knex)
    let questionController = new QuestionController(questionService)
    let roomService = new RoomService(knex);
    let roomController = new RoomController(roomService)

    app.use(userController.router);
    app.use(questionController.router);
    app.use(roomController.router);
}
