import { PythonShell, Options } from "python-shell";

let options: Options = {
	mode: "text",
	pythonOptions: ["-u"], // get print results in real-time
	args: ["value1", "value2", "value3"],
};

export default PythonShell.run("test.py", options, function (err, results) {
	if (err) throw err;
	// results is an array consisting of messages collected during execution
	console.log("results: %j", results);
});
