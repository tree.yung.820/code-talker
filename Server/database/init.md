<!-- 1. CREATE DB in psql -->
psql -U postgres
CREATE DATABASE code_talker

<!-- 2. Change ./env -->
DB_NAME=code_talker
DB_USERNAME=postgres
DB_PASSWORD=postgres
POSTGRES_DB=
POSTGRES_USER=postgres
POSTGRES_PASSWORD=postgres
POSTGRES_HOST=8080
GOOGLE_CLIENT_ID=
GOOGLE_CLIENT_SECRET=
PORT=8080
SECRET=code_talker
NODE_ENV=development


<!-- 3. yarn install -->
yarn install


<!-- 4. knex command -->
yarn knex migrate:latest
yarn knex migrate:rollback
yarn knex seed:run
