import nodemailer from 'nodemailer'
import env from "../util/env";

export async function sendVerificationEmail(receiver: string, user_id: number) {

    // create reusable transporter object using the default SMTP transport
    let mailTransport = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: "codetalker237@gmail.com", // generated gmail user
            pass: "vwdptsybimujudop", // generated gmail account password
        }
    });

    let content = `<p>Thanks for signing up to Code-Talker!<p>
        <p>To get started, click the link below to confirm your account.</p> 
        <p><a href="http://localhost:8080/user/verification/${user_id}">Confirm your account</a></p>`

    // send mail with defined transport object
    let info = await mailTransport.sendMail({
        from: `Code-talker <${env.GMAIL_ACCOUNT}>`, // sender address
        to: receiver, // list of receivers
        subject: "Code-Talker Sign-Up Confirmation", // Subject line
        text: "Hello World", // plain text body
        html: content,
    });

    console.log("Message sent: %s", info.messageId);
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
    return content
}

export async function sendInvitationEmail(email_list: Array<string>, roomURL: string) {

    // create reusable transporter object using the default SMTP transport
    let mailTransport = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: "codetalker237@gmail.com", // generated gmail user
            pass: "vwdptsybimujudop", // generated gmail account password
        }
    });

    let content = `<p>You have been invited!<p>
        <p>To get started, click the link below.</p> 
        <p><a href="${roomURL}">${roomURL}</a></p>`

    // send mail with defined transport object
    for (let email of email_list) {
        let info = await mailTransport.sendMail({
            from: `Code-talker <${env.GMAIL_ACCOUNT}>`, // sender address
            to: email, // list of receivers
            subject: `You have been invited!`, // Subject line
            text: "Hello World", // plain text body
            html: content,
        });
        console.log("Message sent: %s", info.messageId);
    }



    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
    return { content }
}

// let receiver = 'matthewyuen4@gmail.com'
// sendVerificationEmail(receiver)