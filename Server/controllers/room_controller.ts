import { Request, Response } from "express";
import { RoomService } from "../services/room_service";
import { RestfulController } from "./restful_controller";
// import sendVerificationEmail from '../nodemailer/nodemailer'
import { v4 as uuidv4 } from "uuid";
import { Bearer } from "permit";
// import { sendInvitationEmail } from "../nodemailer/nodemailer";
import jwtSimple from "jwt-simple";
import jwt from "../auth/jwt";
import { isLoggedIn } from "../auth/guard";
import { IEmailTemplateContext, LocaleEnum, SendMailOption } from "../services/email_service";
import { getEmailService } from "../util/email";
import { EmailTemplate } from "../util/EmailTemplate";
import * as fs from "fs";
import path from "path";

const permit = new Bearer({
	query: "access_token",
});
export class RoomController extends RestfulController {
	constructor(private roomService: RoomService) {
		super();
		this.router.post("/code-room", this.createRoomAndAssignRoomId);
		this.router.post("/getUserRoomFromDB", this.createRoomInfo);
	}

	createRoomAndAssignRoomId = this.handleRequest(async (req: Request, res: Response) => {
		const token = permit.check(req);
		const loginPayload = jwtSimple.decode(token, jwt.jwtSecret);
		const user_id = loginPayload.user_id;
		const username = loginPayload.username;
		const userEmail = loginPayload.email;
		let { emailList, questionDetails } = req.body;
		// console.log({ questionDetails });
		let questionId = questionDetails.id;
		let roomId = uuidv4();
		let firstPart = "http://localhost:3000/code/";
		let roomURL = firstPart + roomId;

		//turn e-mail array in to below
		console.log("emailList :", emailList);
		// ['shoppingjjtecky@gmail.com','matthewyuen4@gmail.com','codetalker237@gmail.com']
		let IToList = [];
		try {
			for (let email of emailList) {
				if (email !== '') {
					IToList.push({
						email: email,
						locale: LocaleEnum.EN_US,
						context: { roomURL: roomURL },
					});
				}
			}
		} catch (e) {
			console.log(e);
		}
		// console.log(IToList)
		try {
			let emailService = getEmailService();
			let sendMaileOption: SendMailOption<IEmailTemplateContext["INVITATION"]> = {
				IToList,
				template: EmailTemplate.INVITATION,
			};

			//send e-mail
			let sentEmail = emailService.sendEmails(sendMaileOption);
			//create room returning id
			let createdCodeRoom = await this.roomService.createCodeRoom(roomId, user_id, questionId);
			console.log("created room :", createdCodeRoom);
			//insert participate
			await this.roomService.createRoomParticipate(createdCodeRoom.id, userEmail, user_id); //host
			for (let email of emailList) {
				let checkedUserId: any = await this.roomService.checkParticipateUserId(email);
				if (checkedUserId !== undefined) {
					//user
					await this.roomService.createRoomParticipate(createdCodeRoom.id, email, checkedUserId.id);
				} else {
					//guest
					await this.roomService.createRoomParticipate(createdCodeRoom.id, email, null);
				}
			}

			//insert email_delivery_history in DB
			for (let i = 0; i < emailList.length; i++) {
				let email = emailList[i];
				let emailHistory = await this.roomService.createEmailDelivery(
					"system",
					email,
					"Invitation From Code-talker",
					sentEmail.htmlContent[i],
					"delivered"
				);

				await this.roomService.codeRoomInvitation(user_id, createdCodeRoom.id, emailHistory.id);
			}
		} catch (error) {
			console.log("create room and send email fail : ", error);
		}

		return { roomId, roomURL, username };
	});

	createRoomInfo = this.handleRequest(async (req, res) => {
		const token = permit.check(req);
		const loginPayload = jwtSimple.decode(token, jwt.jwtSecret);
		const user_id = loginPayload.user_id;
		const username = loginPayload.username;
		const { roomId } = req.body;
		console.log("roomId : ", roomId);
		let roomInfo: any = await this.roomService.getRoomInfo(roomId);
		// console.log("before roomInfo: ", roomInfo);

		// let questMd = roomInfo.question_md;
		// let roomInfo = await this.roomService.getRoomInfoByUserId(user_id)
		// fs.mkdir(path.join(`code-room/${roomId}`), { recursive: true }, (err) => {
		// 	if (err) {
		// 		throw err;
		// 	} else {
		// 		console.log("mkdir code success");
		// 	}
		// });
		fs.mkdir(path.join(`code-room/${roomId}/test`), { recursive: true }, (err) => {
			if (err) {
				throw err;
			} else {
				console.log("mkdir test success");
			}
		});
		// console.log(questMd);
		roomInfo.forEach((data: any) => {
			const mdContent = fs.readFileSync(path.join(`markdown/${data.question_md}`), { encoding: "utf8" });
			data["mdContent"] = mdContent;
		});
		console.log("roomInfo: ", roomInfo);

		return roomInfo;
		// await roomService
	});

	createRoomWithOutSendEmail = this.handleRequest(async (req: Request, res: Response) => { });
}
