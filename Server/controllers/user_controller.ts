import { Request, Response } from "express";
import { UserService } from "../services/user_service";
import { checkPassword, hashPassword } from "../util/hash";
import { logger } from "../util/logger";
import { Email, HttpError, User } from "../util/models";
import { RestfulController } from "./restful_controller";
import crypto from "crypto";
import { validate } from "../util/helper";
// import { isLoggedIn } from "../middleware/guard";
import { isLoggedIn } from "../auth/guard";
import { form } from "../util/formidable";
import jwtSimple from "jwt-simple";
import jwt from "../auth/jwt";
// import { sendVerificationEmail, sendInvitationEmail } from "../nodemailer/nodemailer";
import { knex } from "../util/db";
import { Bearer } from "permit";
import { EmailService, IEmailTemplateContext, LocaleEnum, SendMailOption } from "../services/email_service";
import { getEmailService } from "../util/email";
import { EmailTemplate } from "../util/EmailTemplate";
import { IncomingMessage } from "http";

const permit = new Bearer({
    query: "access_token",
});
export class UserController extends RestfulController {
    constructor(private userService: UserService) {
        super();
        this.router.get("/testEmail", this.testEmail);
        this.router.get("/user", this.get);
        this.router.post("/user/login", this.login);
        this.router.post("/user/loginWithToken", this.loginWithToken);
        this.router.post("/user/register", this.register);
        this.router.get("/user/verification/:user_id", this.verification);
        this.router.get("/user/login/google", this.googleLogin);
        this.router.get("/user/logout", isLoggedIn, this.logout);
        this.router.put("/user/update", isLoggedIn, this.updateUser);
        this.router.get("/userData", isLoggedIn, this.userData);
        this.router.get("/userQuestionStatus", isLoggedIn, this.userQuestionStatus)
        //testing
        this.router.get("/userProfile", this.userProfile);
    }

    testEmail = (_: Request, res: Response) => {
        try {
            let emailService = getEmailService();
            let sendMaileOption: SendMailOption<IEmailTemplateContext["VERIFICATION"]> = {
                IToList: [
                    {
                        email: "matthewyuen4@gmail.com",
                        locale: LocaleEnum.ZH_TW,
                        context: { userId: 9999 },
                    },
                    {
                        email: "matthewyuen4@gmail.com",
                        locale: LocaleEnum.EN_US,
                        context: { userId: 200000 },
                    },
                ],
                template: EmailTemplate.REGISTRATION,
            };

            emailService.sendEmails(sendMaileOption);
            res.end("ok");
        } catch (error) {
            console.log("send emal fail : ", error);
            res.end("not ok");
        }
    };

    get = (req: Request, res: Response) => {
        // set user theme
        req.session && req.session["user"] ? res.json({ info: req.session["user"] }) : res.json({ info: null });
    };

    login = this.handleRequest(async (req: Request, res: Response) => {
        console.log("body", req.body);
        validate(req.body);
        let { email, password } = req.body;

        let foundUser: User;
        logger.debug("%o", req.body);

        /@/.test(email)
            ? (foundUser = await this.userService.getUserByEmail(email))
            : (foundUser = await this.userService.getUserByUsername(email));

        if (!foundUser) throw new HttpError(400, "Invalid username or password.");
        logger.debug("check !found");

        console.log({ foundUser })
        if (foundUser.status !== 'active') {
            logger.debug("check !account status")

            throw new HttpError(400, "Your account is not activated!");
        }
        let isPasswordValid = await checkPassword(password, foundUser.password!);
        if (!isPasswordValid) {
            logger.debug("check !password");

            throw new HttpError(400, "Invalid username or password.");
        }

        delete foundUser["password"];
        logger.info("%o", foundUser);

        req.session && (req.session["user"] = foundUser);

        const payload = {
            user_id: foundUser.id,
            role: foundUser.role,
            username: foundUser.username,
            email: foundUser.email,
            status: foundUser.status,
        };

        console.log("payload :", payload);
        //send token to guard payload
        const token = jwtSimple.encode(payload, jwt.jwtSecret);

        return { token, message: `User ${foundUser.username} logged in.` };
    });

    loginWithToken = this.handleRequest(async (req: Request, res: Response) => {
        console.log("body", req.body);
        validate(req.body);

        logger.debug("%o", req.body);

        const loginPayload = jwtSimple.decode(req.body.token, jwt.jwtSecret);
        const user_id = loginPayload.user_id;
        const foundUser = (await knex.table("users").where("id", user_id))[0];

        if (!foundUser) throw new HttpError(400, "Invalid username or password.");
        logger.debug("check !found");

        delete foundUser["password"];
        logger.info("%o", foundUser);

        req.session && (req.session["user"] = foundUser);

        const payload = {
            user_id: foundUser.id,
            role: foundUser.role,
            username: foundUser.username,
            email: foundUser.email,
            status: foundUser.status,
        };

        //send token to guard payload
        const token = jwtSimple.encode(payload, jwt.jwtSecret);

        return { token, message: `User ${foundUser.username} logged in.` };
    });

    register = this.handleRequest(async (req: Request, res: Response) => {
        let { fields, file } = await PromiseFormParse(req)
        console.log({ fields, file })
        let profile = file.profile

        let { username, password, email } = fields
        console.log({ fields })

        password = await hashPassword(password.toString());

        //insert new user in DB
        let avatar = file.profile.newFilename
        let newUser: User = await this.userService.addUser(username, password, email, avatar);

        //send verification email to user
        // let content = await sendVerificationEmail(email, newUser.id);

        try {
            let emailService = getEmailService();
            let sendMaileOption: SendMailOption<IEmailTemplateContext["VERIFICATION"]> = {
                IToList: [
                    {
                        email: email,
                        locale: LocaleEnum.EN_US,
                        context: { userId: newUser.id },
                    }
                ],
                template: EmailTemplate.VERIFICATION,
            };

            let sentEmail = emailService.sendEmails(sendMaileOption);

            //insert email_delivery_history in DB
            let emailHistory: Email = await this.userService.createEmailDelivery(
                "system",
                newUser.email,
                "Code-Talker Sign-Up Confirmation",
                sentEmail.htmlContent[0],
                "delivered"
            );

            //inset user_email_verification in DB
            await this.userService.createEmailVerify(newUser.id, emailHistory.id, "none");

        } catch (error) {
            console.log("send email fail : ", error);
        }

        // req.session && (req.session["user"] = newUser);

        const payload = {
            user_id: newUser.id,
            role: newUser.role,
            username: newUser.username,
            email: newUser.email,
            status: newUser.status,
        };

        //send token to guard payload
        const token = jwtSimple.encode(payload, jwt.jwtSecret);

        return { message: `user ${newUser.username} registered`, token };


    });

    verification = this.handleRequest(async (req: Request, res: Response) => {
        let user_id: any = req.params.user_id;
        await this.userService.updateVerifyStatus(user_id);
        res.redirect("http://localhost:3000/home");
        console.log("email verified");
        //change status from pending to verified
        return;
    });

    googleLogin = async (req: Request, res: Response) => {
        let accessToken = req.session?.["grant"].response.access_token;
        let googleUserInfo = await this.userService.getGoogleInfo(accessToken);
        let foundUser: User = await this.userService.getUserByEmail(googleUserInfo.email);
        if (!foundUser) {
            let googleAccount = {
                username: googleUserInfo.name.concat(Date.now()),
                password: await hashPassword(crypto.randomBytes(20).toString("hex")),
                email: googleUserInfo.email,
                avatar: googleUserInfo.picture,
                status: "pending",
            };
            foundUser = await this.userService.addUser(
                googleAccount.username,
                googleAccount.password,
                googleAccount.email,
                googleAccount.avatar
            );
        }
        req.session["user"] = foundUser;
        res.redirect("/index.html");
    };

    logout = (req: Request, res: Response) => {
        delete req.session["user"];
        console.log("logout successful");
        res.redirect("http://localhost:3000/index");
    };

    updateUser = this.handleRequest(async (req: Request, res: Response) => {
        const user_id = req.session["user"].id;
        form.parse(req, async (err, field, files) => {
            //check picture file exist or not
            let file = Array.isArray(files.image) ? files.image[0] : files.image;
            let image = file.newFilename;
            let fileName = Object.keys(files).length === 0 ? null : image;
            //check password and confirm password have same input
            if (field.password != field.confirmPassword) {
                logger.debug("check !password");
                throw new HttpError(400, "Invalid password.");
            }

            let hashedPassword = field.password ? await hashPassword(field.password as string) : null;

            await this.userService.updateSetting(field.username as string, hashedPassword, fileName, user_id);
            return;
        });
    });

    userData = this.handleRequest(async (req: Request, res: Response) => {
        const token = permit.check(req);
        const loginPayload = jwtSimple.decode(token, jwt.jwtSecret);
        const user_id = loginPayload.user_id;
        let userInfo = await this.userService.getUserInfo(user_id);
        let favorites = await this.userService.getUserFavorite(user_id);
        let history = await this.userService.getUserHistory(user_id);

        return { userInfo, favorites, history };
    });

    // testing
    userProfile = this.handleRequest(async (req: Request, res: Response) => {
        // const user = req.user
        // from guard req.user
        // console.log(x)
        return
    });
    // -------

    userQuestionStatus = this.handleRequest(async (req: Request, res: Response) => {
        let { user_id, question_id } = req.body
        // let { user_id, question_id } = {
        //     user_id: 5,
        //     question_id: 1,
        // }
        let userQuestionStatus = await this.userService.getQuestionStatusByUser(user_id, question_id)
        return userQuestionStatus
    });

    async validateInput(req: Request) {
        validate(req.body);
        let { username, email, password, confirmPassword } = req.body;

        let whiteSpace = /^\s+/;
        if (username.match(whiteSpace) || email.match(whiteSpace) || password.match(whiteSpace)) {
            return new HttpError(400, "Cannot into space.");
        }

        if (username.match(/@/)) {
            return new HttpError(400, "Cannot use @ in username.");
        }
        if (password !== confirmPassword) {
            return new HttpError(400, "The passwords you entered do not match.");
        }

        let user = await this.userService.getUserByUsername(username);
        if (user) return new HttpError(400, "Username has been used.");
        let userEmail = await this.userService.getUserByEmail(email);
        if (userEmail) return new HttpError(400, "This email address is not available. Choose a different address.");
        return;
    }
}


async function PromiseFormParse(request: IncomingMessage) {
    return new Promise<any>((resolve, reject) => {
        form.parse(request, async (err, fields, files) => {
            if (err) {
                reject(err)
            }
            let file = Array.isArray(files) ? files[0] : files;

            if (file == null) throw new HttpError(400, "no image received");
            resolve({
                fields,
                file
            });
        });
    });
}
