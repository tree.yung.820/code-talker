import { Request, Response } from "express";
import { QuestionService } from "../services/question.service";
import { checkPassword, hashPassword } from "../util/hash";
import { logger } from "../util/logger";
import { Email, HttpError, User } from "../util/models";
import { RestfulController } from "./restful_controller";
import crypto from "crypto";
import { validate } from "../util/helper";
// import { isLoggedIn } from "../middleware/guard";
import { isLoggedIn } from "../auth/guard";
import { form } from "../util/formidable";
import jwtSimple from "jwt-simple";
import jwt from "../auth/jwt";
// import sendVerificationEmail from '../nodemailer/nodemailer'
import { knex } from "../util/db";
import { Bearer } from "permit";;
const permit = new Bearer({
    query: "access_token"
})

export class QuestionController extends RestfulController {
    constructor(private questionService: QuestionService) {
        super();
        this.router.get("/question", this.questionsWithoutLoggedIn);
        this.router.post("/addQuestionToFav", isLoggedIn, this.questionAddToFav)
        this.router.post("/deleteQuestionToFav", isLoggedIn, this.questionDeleteToFav)
        this.router.get("/question-details/:questionId", isLoggedIn, this.questionDetails)
        this.router.get('/popular-question', this.popularQuestion)
        // this.router.put("/user/update", isLoggedIn, this.questions);
    }

    questionsWithoutLoggedIn = this.handleRequest(async (req: Request, res: Response) => {
        let question = await this.questionService.getQuestionsFromDB()
        return question
    });

    questionAddToFav = this.handleRequest(async (req: Request, res: Response) => {
        const { questionDetails } = req.body
        let userId = req!.user!.user_id
        console.log("question_id", questionDetails.question_id)
        let question_id = questionDetails.id
        await this.questionService.insertQuestionToDB(userId, question_id)
        return { userId, question_id }
    })

    questionDeleteToFav = this.handleRequest(async (req: Request, res: Response) => {
        const { questionDetails } = req.body
        // const token = permit.check(req);
        // const loginPayload = jwtSimple.decode(token, jwt.jwtSecret);
        // const userId = loginPayload.user_id;
        let userId = req!.user!.user_id
        console.log("question details of delete", questionDetails)

        console.log("question_id", questionDetails.question_id)
        let question_id = questionDetails.id
        await this.questionService.deleteQuestionToDB(userId, question_id)
        return { userId, question_id }
    })



    questionDetails = this.handleRequest(async (req: Request, res: Response) => {
        let { questionId } = req.params
        let userId = req!.user!.user_id
        if (!Number(questionId) || !userId) {
            res.status(400).json({
                message: "Invalid input"
            })
            return
        }
        let questionDetails = await this.questionService.getQuestionDetails(Number(questionId), userId)
        return questionDetails
    })

    popularQuestion = this.handleRequest(async (req: Request, res: Response) => {

        let generalPopularQuestion = await this.questionService.getGeneralPopularQuestion()
        let easyPopularQuestion = await this.questionService.getEasyPopularQuestion()
        let mediumPopularQuestion = await this.questionService.getMediumPopularQuestion()
        let hardPopularQuestion = await this.questionService.getHardPopularQuestion()

        return { generalPopularQuestion, easyPopularQuestion, mediumPopularQuestion, hardPopularQuestion }
    })
}
