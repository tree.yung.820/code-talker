import express, { Request, Response, NextFunction } from "express";

export class RestfulController {
	router = express.Router();

	handleRequest(fn: (req: Request, res: Response, next: NextFunction) => Promise<any>) {
		return async (req: Request, res: Response, next: NextFunction) => {
			try {
				let json = await fn(req, res, next);
				res.json(json);
			} catch (error: any) {
				console.log(error);
				if ("status" in error && "message" in error) {
					res.status(error.status).json(error.message);
				} else {
					res.status(500).json(error);
				}
			}
		};
	}
}
