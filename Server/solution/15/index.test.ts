import { longestConsecutive } from "../index";

describe("test q1", () => {
	it("Input: nums = [100,4,200,1,3,2] can output 4", () => {
		expect(longestConsecutive([100, 4, 200, 1, 3, 2])).toStrictEqual(4);
	});

	it("nums = [0,3,7,2,5,8,4,6,0,1] can output 9", () => {
		expect(longestConsecutive([0, 3, 7, 2, 5, 8, 4, 6, 0, 1])).toStrictEqual(9);
	});
});
