import { fibonacci } from "../index";

describe("testing on fibonacci", () => {
	// Positive
	it("Input: n = 2 can output 1", () => {
		expect(fibonacci(1)).toBe(1);
	});
	it("Input: n = 10 can output 55", () => {
		expect(fibonacci(10)).toBe(55);
	});
	// Negative
	it("Input: n = 201 can output Error", () => {
		expect(() => {
			fibonacci(201);
		}).toThrow();
	});
	it("Input: n = 5.6 can output Error", () => {
		expect(() => {
			fibonacci(5.6);
		}).toThrow();
	});
	it("Input: n = Number.POSITIVE_INFINITY || Number.NEGATIVE_INFINITY can output Error", () => {
		expect(() => {
			fibonacci(Number.POSITIVE_INFINITY);
		}).toThrow();

		expect(() => {
			fibonacci(Number.NEGATIVE_INFINITY);
		}).toThrow();
	});
});
