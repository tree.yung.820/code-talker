import { findPeakElement } from "../index";

describe("test q1", () => {
	it("Input: nums = [1,2,3,1] can output 2", () => {
		expect(findPeakElement([1, 2, 3, 1])).toStrictEqual(2);
	});

	it("Input: nums = [1,2,1,3,5,6,4] can output 5", () => {
		expect(findPeakElement([1, 2, 1, 3, 5, 6, 4])).toStrictEqual(5);
	});
});
