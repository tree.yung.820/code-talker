import { fourSum } from "../index";

describe("test q1", () => {
	it("Input: nums = [1,0,-1,0,-2,2], target = 0 can output [[-2,-1,1,2],[-2,0,0,2],[-1,0,0,1]]", () => {
		expect(fourSum([1, 0, -1, 0, -2, 2], 0)).toStrictEqual([
			[-2, -1, 1, 2],
			[-2, 0, 0, 2],
			[-1, 0, 0, 1],
		]);
	});

	it("Input: nums = [2,2,2,2,2], target = 8 can output [[2,2,2,2]]", () => {
		expect(fourSum([2, 2, 2, 2, 2], 8)).toStrictEqual([[2, 2, 2, 2]]);
	});
});
