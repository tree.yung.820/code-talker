import { threeSum } from "../index";

describe("test q1", () => {
	it("input [-1, 0, 1, 2, -1, -4] can output [[-1,-1,2],[-1,0,1]]", () => {
		expect(threeSum([-1, 0, 1, 2, -1, -4])).toStrictEqual([
			[-1, -1, 2],
			[-1, 0, 1],
		]);
	});

	it("input [0, 1, 1] can output []", () => {
		expect(threeSum([0, 1, 1])).toStrictEqual([]);
	});

	it("input [0,0,0] can output [0,0,0]", () => {
		expect(threeSum([0, 0, 0])).toStrictEqual([[0, 0, 0]]);
	});
});
