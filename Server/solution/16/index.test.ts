import { maximumGap } from "../index";

describe("test q1", () => {
	it("Input: nums = [3,6,9,1] can output 3", () => {
		expect(maximumGap([3, 6, 9, 1])).toStrictEqual(3);
	});

	it("nums = [10] can output 0", () => {
		expect(maximumGap([10])).toStrictEqual(0);
	});
});
