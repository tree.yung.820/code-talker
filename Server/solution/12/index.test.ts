import { fractionToDecimal } from "../index";

describe("test q1", () => {
	it("Input: numerator = 1, denominator = 2 can output '0.5'", () => {
		expect(fractionToDecimal(1, 2)).toStrictEqual("0.5");
	});

	it("Input: numerator = 2, denominator = 1 can output '2'", () => {
		expect(fractionToDecimal(2, 1)).toStrictEqual(2);
	});

	it("Input: numerator = 4, denominator = 333 can output '0.(012)'", () => {
		expect(fractionToDecimal(4, 333)).toStrictEqual("0.(012)");
	});
});
