import { removeDuplicateLetters } from "../index";

describe("test q1", () => {
	it("Input: s = 'bcabc' can output 'abc'", () => {
		expect(removeDuplicateLetters("bcabc")).toStrictEqual("abc");
	});

	it("s = 'cbacdcbc' can output 'acdb'", () => {
		expect(removeDuplicateLetters("cbacdcbc")).toStrictEqual("acdb");
	});
});
