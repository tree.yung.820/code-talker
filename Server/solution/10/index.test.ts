import { numDecodings } from "../index";

describe("test q1", () => {
	it("Input: s = '12' can output 2", () => {
		expect(numDecodings("12")).toStrictEqual(2);
	});

	it("Input: s = '226' can output 3", () => {
		expect(numDecodings("226")).toStrictEqual(3);
	});
	it("Input: s = '06' can output 0", () => {
		expect(numDecodings("06")).toStrictEqual(0);
	});
});
