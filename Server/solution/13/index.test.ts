import { strStr } from "../index";

describe("test q1", () => {
	it("Input: haystack = 'hello', needle = 'll' can output 2", () => {
		expect(strStr("hello", "ll")).toStrictEqual(2);
	});

	it("Input: haystack = 'aaaaa', needle = 'bba' can output -1", () => {
		expect(strStr("aaaaa", "bba")).toStrictEqual(-1);
	});
});
