import { canFinish } from "../index";

describe("test q1", () => {
	it("Input: numCourses = 2, prerequisites = [[1,0]] can output true", () => {
		expect(canFinish(2)).toStrictEqual([[1, 0]]);
	});

	it("Input: numCourses = 2, prerequisites = [[1,0],[0,1]] can output false", () => {
		expect(canFinish(2)).toStrictEqual([
			[1, 0],
			[0, 1],
		]);
	});
});
