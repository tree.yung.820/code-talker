import { rotate } from "../index";

describe("test q1", () => {
	it("Input: nums = [1,2,3,4,5,6,7], k = 3 can output [5,6,7,1,2,3,4]", () => {
		expect(rotate([1, 2, 3, 4, 5, 6, 7], 3)).toStrictEqual([5, 6, 7, 1, 2, 3, 4]);
	});

	it("Input: nums = [-1,-100,3,99], k = 2 can output[3,99,-1,-100]", () => {
		expect(rotate([-1, -100, 3, 99], 2)).toStrictEqual([3, 99, -1, -100]);
	});
});
