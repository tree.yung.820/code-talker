import { addTwoNumbers } from "../index";

describe("test q1", () => {
	it("Input: l1 = [2,4,3], l2 = [5,6,4] can output [7,0,8]", () => {
		expect(addTwoNumbers([2, 4, 3], [5, 6, 4])).toStrictEqual([7, 0, 8]);
	});

	it("Input: l1 = [0], l2 = [0] can output [0]", () => {
		expect(addTwoNumbers([0], [0])).toStrictEqual([0]);
	});

	it("Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9] can output [8,9,9,9,0,0,0,1]", () => {
		expect(addTwoNumbers([9, 9, 9, 9, 9, 9, 9], [9, 9, 9, 9])).toStrictEqual([8, 9, 9, 9, 0, 0, 0, 1]);
	});
});
