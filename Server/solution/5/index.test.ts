import { maxProfit } from "../index";

describe("test q1", () => {
	it("Input: prices = [7,1,5,3,6,4] can output 5", () => {
		expect(maxProfit([7, 1, 5, 3, 6, 4])).toStrictEqual(5);
	});

	it("Input: prices = [7,6,4,3,1] can output 0", () => {
		expect(maxProfit([7, 6, 4, 3, 1])).toStrictEqual(0);
	});
});
