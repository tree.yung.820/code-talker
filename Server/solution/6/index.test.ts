import { candy } from "../index";

describe("test q1", () => {
	it("Input: ratings = [1,0,2] can output 5", () => {
		expect(candy([1, 0, 2])).toStrictEqual(5);
	});

	it("ratings = [1,2,2] can output 4", () => {
		expect(candy([1, 2, 2])).toStrictEqual(4);
	});
});
