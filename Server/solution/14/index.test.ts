import { canJump } from "../index";

describe("test q1", () => {
	it("Input: nums = [2,3,1,1,4] can output true", () => {
		expect(canJump([2, 3, 1, 1, 4])).toStrictEqual(true);
	});

	it("Input: nums = [3,2,1,0,4] can output false", () => {
		expect(canJump([3, 2, 1, 0, 4])).toStrictEqual(false);
	});
});
