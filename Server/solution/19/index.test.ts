import { restoreIpAddresses } from "../index";

describe("test q1", () => {
	it("Input: s = '25525511135' can output ['255.255.11.135','255.255.111.35']", () => {
		expect(restoreIpAddresses("25525511135")).toStrictEqual(["255.255.11.135", "255.255.111.35"]);
	});

	it("Input: s = '0000' can output ['0.0.0.0']", () => {
		expect(restoreIpAddresses("0000")).toStrictEqual(["0.0.0.0"]);
	});
	it("Input: s = '101023' can output [1.0.10.23,1.0.102.3,10.1.0.23,10.10.2.3,101.0.2.3]", () => {
		expect(restoreIpAddresses("25525511135")).toStrictEqual(["255.255.11.135", "255.255.111.35"]);
	});
});
