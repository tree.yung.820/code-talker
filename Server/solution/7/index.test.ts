import { combinationSum } from "../index";

describe("test q1", () => {
	it("Input: candidates = [2,3,6,7], target = 7 can output [[2,2,3],[7]]", () => {
		expect(combinationSum([2, 3, 6, 7], 7)).toStrictEqual([[2, 2, 3], [7]]);
	});

	it("Input: candidates = [2,3,5], target = 8 can output [[2,2,2,2],[2,3,3],[3,5]]", () => {
		expect(combinationSum([2, 3, 5], 8)).toStrictEqual([
			[2, 2, 2, 2],
			[2, 3, 3],
			[3, 5],
		]);
	});
	it("Input: candidates = [2], target = 1 can output []", () => {
		expect(combinationSum([2], 1)).toStrictEqual([]);
	});
});
