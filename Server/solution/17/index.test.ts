import { plusOne } from "../index";

describe("test q1", () => {
	it("Input: digits = [1,2,3] can output [1,2,4]", () => {
		expect(plusOne([1, 2, 3])).toStrictEqual([1, 2, 4]);
	});

	it("nums = [4,3,2,1] can output [4,3,2,2]", () => {
		expect(plusOne([4, 3, 2, 1])).toStrictEqual([4, 3, 2, 2]);
	});
});
