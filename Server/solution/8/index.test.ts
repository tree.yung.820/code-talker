import { containsDuplicate } from "../index";

describe("test q1", () => {
	it("Input: nums = [1,2,3,1] can output true", () => {
		expect(containsDuplicate([1, 2, 3, 1])).toStrictEqual(true);
	});

	it("Input: nums = [1,2,3,4] can output false", () => {
		expect(containsDuplicate([1, 2, 3, 4])).toStrictEqual(false);
	});
	it("Input: nums = [1,1,1,3,3,4,3,2,4,2] can output true", () => {
		expect(containsDuplicate([1, 1, 1, 3, 3, 4, 3, 2, 4, 2])).toStrictEqual(true);
	});
});
