import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {


    if (!(await knex.schema.hasTable("difficulties"))) {
        await knex.schema.createTable("difficulties", (table) => {
            table.increments();
            table.string("difficulty").notNullable();
            table.timestamps(false, true);
        });
    }

    if (!(await knex.schema.hasTable("programming_languages"))) {
        await knex.schema.createTable("programming_languages", (table) => {
            table.increments();
            table.string("programming_language").notNullable();
            table.string("extension").nullable();
            table.timestamps(false, true);
        });
    }

    if (!(await knex.schema.hasTable("categories"))) {
        await knex.schema.createTable("categories", (table) => {
            table.increments();
            table.string("name").notNullable();
            table.timestamps(false, true);
        });
    }

    if (!(await knex.schema.hasTable("users"))) {
        await knex.schema.createTable("users", (table) => {
            table.increments();
            table.string("username").notNullable();
            table.string("password").notNullable();
            table.string("email").notNullable();
            table.string("role").notNullable().defaultTo("user");
            table.text("avatar").notNullable().defaultTo("avatar.jpg");
            table.string("status").notNullable().defaultTo("pending");
            table.timestamps(false, true);
        });
    }

    if (!(await knex.schema.hasTable("questions"))) {
        await knex.schema.createTable("questions", (table) => {
            table.increments();
            table.string("title").notNullable();
            table.text("question").notNullable();
            table.text("question_md").notNullable();
            table.text("validation_file").notNullable();
            table.integer("programming_languages_id").unsigned();
            table.foreign("programming_languages_id").references("programming_languages.id");
            table.integer("created_by_user_id").unsigned();
            table.foreign("created_by_user_id").references("users.id");
            table.integer("difficulties_id").unsigned();
            table.foreign("difficulties_id").references("difficulties.id");
            table.integer("categories_id").unsigned();
            table.foreign("categories_id").references("categories.id");
            table.timestamps(false, true);
        });
    }

    if (!(await knex.schema.hasTable("user_completion_history"))) {
        await knex.schema.createTable("user_completion_history", (table) => {
            table.increments();
            table.integer("user_id").unsigned();
            table.foreign("user_id").references("users.id");
            table.integer("question_id").unsigned();
            table.foreign("question_id").references("questions.id");
            table.timestamp("start_time").notNullable();
            table.text("answer").notNullable();
            table.timestamps(false, true);
        });
    }

    if (!(await knex.schema.hasTable("user_favoured"))) {
        await knex.schema.createTable("user_favoured", (table) => {
            table.increments();
            table.integer("user_id").unsigned();
            table.foreign("user_id").references("users.id");
            table.integer("question_id").unsigned();
            table.foreign("question_id").references("questions.id");
            table.timestamps(false, true);
        });
    }

    if (!(await knex.schema.hasTable("email_delivery_history"))) {
        await knex.schema.createTable("email_delivery_history", (table) => {
            table.increments();
            table.string("from").notNullable();
            table.string("to").notNullable();
            table.string("title").notNullable();
            table.text("content").notNullable();
            table.string("status").notNullable();
            table.timestamps(false, true);
        });
    }

    if (!(await knex.schema.hasTable("code_room"))) {
        await knex.schema.createTable("code_room", (table) => {
            table.increments();
            table.string("room_id").notNullable();
            table.integer("delivery_history_id").unsigned();
            table.foreign("delivery_history_id").references("email_delivery_history.id");
            table.integer("created_by_user_id").unsigned();
            table.foreign("created_by_user_id").references("users.id");
            table.integer("question_id").unsigned();
            table.foreign("question_id").references("questions.id");
            table.string("status").notNullable().defaultTo("open");
            table.timestamps(false, true);
        });
    }

    if (!(await knex.schema.hasTable("user_email_verification"))) {
        await knex.schema.createTable("user_email_verification", (table) => {
            table.increments();
            table.integer("user_id").unsigned();
            table.foreign("user_id").references("users.id");
            table.integer("delivery_history_id").unsigned();
            table.foreign("delivery_history_id").references("email_delivery_history.id");
            table.string("token").nullable();
            table.string("status").notNullable().defaultTo("pending");;
            table.timestamps(false, true);
        });
    }

    if (!(await knex.schema.hasTable("room_participants"))) {
        await knex.schema.createTable("room_participants", (table) => {
            table.increments();
            table.integer("code_room_id").unsigned();
            table.foreign("code_room_id").references("code_room.id");
            table.string("participant").notNullable();
            table.integer("user_id").nullable()
            table.foreign("user_id").references("users.id");
            table.timestamps(false, true);
        });
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("room_participants");
    await knex.schema.dropTableIfExists("user_email_verification");
    await knex.schema.dropTableIfExists("code_room");
    await knex.schema.dropTableIfExists("email_delivery_history");
    await knex.schema.dropTableIfExists("user_favoured");
    await knex.schema.dropTableIfExists("user_completion_history");
    await knex.schema.dropTableIfExists("questions");
    await knex.schema.dropTableIfExists("users");
    await knex.schema.dropTableIfExists("categories");
    await knex.schema.dropTableIfExists("programming_languages");
    await knex.schema.dropTableIfExists("difficulties");
}

