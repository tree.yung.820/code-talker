import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable("user_completion_history")) {
        await knex.schema.table("user_completion_history", (table) => {
            table.dropColumn('start_time')
            table.dropColumn('answer')
            table.string("complete_status").notNullable()
        });
    }
}


export async function down(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable("user_completion_history")) {
        await knex.schema.table("user_completion_history", (table) => {
            table.dropColumn('complete_status')
            table.text("answer").notNullable();
            table.timestamp("start_time")
        });
    }
}
