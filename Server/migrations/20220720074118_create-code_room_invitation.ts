import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {

    if (await knex.schema.hasTable("code_room")) {
        await knex.schema.table("code_room", (table) => {
            table.dropColumn("delivery_history_id")
        });
    }

    if (!(await knex.schema.hasTable("code_room_invitation"))) {
        await knex.schema.createTable("code_room_invitation", (table) => {
            table.increments();
            table.integer("host_user_id").unsigned();
            table.foreign("host_user_id").references("users.id");
            table.integer("code_room_id").unsigned();
            table.foreign("code_room_id").references("code_room.id");
            table.integer("delivery_history_id").unsigned();
            table.foreign("delivery_history_id").references("email_delivery_history.id");
            table.timestamps(false, true);
        });
    }
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("code_room_invitation");

    if (await knex.schema.hasTable("code_room")) {
        await knex.schema.table("code_room", (table) => {
            table.integer("delivery_history_id").unsigned();
            table.foreign("delivery_history_id").references("email_delivery_history.id");
        });
    }
}

