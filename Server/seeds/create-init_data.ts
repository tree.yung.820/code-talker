import { Knex } from "knex";
import XLSX from "xlsx";
// import {} from "../util/models";
import { hashPassword } from "../util/hash";


export async function seed(knex: Knex): Promise<void> {
    const txn = await knex.transaction();
    try {
        let workbook = XLSX.readFile("./seeds/seed_data.xlsx");

        let difficulties_Ws = workbook.Sheets["difficulties"];
        let programming_languages_Ws = workbook.Sheets["programming_languages"];
        let categories_Ws = workbook.Sheets["categories"];
        let users_Ws = workbook.Sheets["users"];
        let questions_Ws = workbook.Sheets["questions"];
        let user_completion_history_Ws = workbook.Sheets["user_completion_history"];
        let user_favoured_Ws = workbook.Sheets["user_favoured"];
        let code_room_Ws = workbook.Sheets["code_room"];
        let email_delivery_history_Ws = workbook.Sheets["email_delivery_history"];
        let user_email_verification_Ws = workbook.Sheets["user_email_verification"];
        let room_participants_Ws = workbook.Sheets["room_participants"];
        let code_room_invitation_Ws = workbook.Sheets["code_room_invitation"];

        let difficulties = XLSX.utils.sheet_to_json(difficulties_Ws);
        let programming_languages = XLSX.utils.sheet_to_json(programming_languages_Ws);
        let categories = XLSX.utils.sheet_to_json(categories_Ws);
        let users: any = XLSX.utils.sheet_to_json(users_Ws);
        let questions = XLSX.utils.sheet_to_json(questions_Ws);
        let user_completion_history = XLSX.utils.sheet_to_json(user_completion_history_Ws);
        let user_favoured = XLSX.utils.sheet_to_json(user_favoured_Ws);
        let code_room = XLSX.utils.sheet_to_json(code_room_Ws);
        let email_delivery_history = XLSX.utils.sheet_to_json(email_delivery_history_Ws);
        let user_email_verification = XLSX.utils.sheet_to_json(user_email_verification_Ws);
        let room_participants = XLSX.utils.sheet_to_json(room_participants_Ws);
        let code_room_invitation = XLSX.utils.sheet_to_json(code_room_invitation_Ws);

        for (let user of users) {
            user.password = await hashPassword(user.password!.toString());
        }

        await txn("code_room_invitation").del();
        await txn.raw("ALTER SEQUENCE code_room_invitation_id_seq RESTART");
        await txn("user_email_verification").del();
        await txn.raw("ALTER SEQUENCE user_email_verification_id_seq RESTART");
        await txn("room_participants").del();
        await txn.raw("ALTER SEQUENCE room_participants_id_seq RESTART");
        await txn("code_room").del();
        await txn.raw("ALTER SEQUENCE code_room_id_seq RESTART");
        await txn("email_delivery_history").del();
        await txn.raw("ALTER SEQUENCE email_delivery_history_id_seq RESTART");
        await txn("user_favoured").del();
        await txn.raw("ALTER SEQUENCE user_favoured_id_seq RESTART");
        await txn("user_completion_history").del();
        await txn.raw("ALTER SEQUENCE user_completion_history_id_seq RESTART");
        await txn("questions").del();
        await txn.raw("ALTER SEQUENCE questions_id_seq RESTART");
        await txn("users").del(); ``
        await txn.raw("ALTER SEQUENCE users_id_seq RESTART");
        await txn("categories").del();
        await txn.raw("ALTER SEQUENCE categories_id_seq RESTART");
        await txn("programming_languages").del();
        await txn.raw("ALTER SEQUENCE programming_languages_id_seq RESTART");
        await txn("difficulties").del();
        await txn.raw("ALTER SEQUENCE difficulties_id_seq RESTART");

        await txn("difficulties").insert(difficulties).returning("id")
        await txn("programming_languages").insert(programming_languages).returning("id")
        await txn("categories").insert(categories).returning("id")
        await txn("users").insert(users).returning("id")
        await txn("questions").insert(questions).returning("id")
        await txn("user_completion_history").insert(user_completion_history).returning("id")
        await txn("user_favoured").insert(user_favoured).returning("id")
        await txn("email_delivery_history").insert(email_delivery_history).returning("id")
        await txn("code_room").insert(code_room).returning("id")
        await txn("user_email_verification").insert(user_email_verification).returning("id")
        await txn("room_participants").insert(room_participants).returning("id")
        await txn("code_room_invitation").insert(code_room_invitation).returning("id")

        await txn.commit();
    } catch (error) {
        console.log(error);
        await txn.rollback();
    }
};
