import { Knex } from "knex";
import { camelCaseKeys } from "../util/helper";
import fetch from "node-fetch";

export class UserService {
	constructor(private knex: Knex) { }

	async getUserByUsername(username: string) {
		return (
			await this.knex("users")
				.select("id", "username", "email", "password", "avatar", "role", "status")
				.where("username", username)
		)[0];
	}

	async getUserByEmail(email: string) {
		return (
			await this.knex("users")
				.select("id", "username", "email", "password", "avatar", "role", "status")
				.where("email", email)
		)[0];
	}

	async addUser(username: string, password: string, email: string, avatar: string | null) {
		return (
			await this.knex("users")
				.insert({ username, password, email, avatar })
				.returning(["id", "username", "email", "avatar", "role", "status"])
		)[0];
	}

	async createEmailDelivery(from: string, to: string, title: string, content: string, status: string) {
		return (
			await this.knex("email_delivery_history")
				.insert({ from, to, title, content, status })
				.returning(["id", "from", "to", "title", "content", "status"])
		)[0]
	}

	async createEmailVerify(user_id: number, delivery_history_id: number, token: string) {
		return (
			await this.knex("user_email_verification")
				.insert({ user_id, delivery_history_id, token })
		)
	}

	async updateVerifyStatus(user_id: number) {
		await this.knex("user_email_verification")
			.where({ user_id: user_id })
			.update({ status: "verified" })
		await this.knex("users")
			.where({ id: user_id })
			.update({ status: "active" })
		return
	}

	async getUserFavorite(user_id: number) {
		return (
			await this.knex("user_favoured")
				.select("questions.id", "title", "question",
					"question_md", "programming_languages.programming_language",
					"users.username", "difficulties.difficulty",
					"categories.name as category")
				.join("questions", "user_favoured.question_id", "=", "questions.id")
				.join("programming_languages", "questions.programming_languages_id", "=", "programming_languages.id")
				.join("users", "questions.created_by_user_id", "=", "users.id")
				.join("difficulties", "questions.difficulties_id", "=", "difficulties.id")
				.join("categories", "questions.categories_id", "=", "categories.id")
				.where({ user_id: user_id })
		)
	}

	async getUserInfo(user_id: number) {
		return (
			await this.knex("users")
				.select("username", "role", "avatar", "status")
				.where({ id: user_id })
		)
	}

	async getUserHistory(user_id: number) {
		return (
			await this.knex("user_completion_history")
				.select("question_id", "title", "complete_status", "user_completion_history.created_at")
				.join('questions', 'user_completion_history.question_id', '=', 'questions.id')
				.where({ user_id: user_id })
		)
	}

	async getGoogleInfo(accessToken: string) {
		const res = await fetch("https://www.googleapis.com/oauth2/v2/userinfo", {
			method: "get",
			headers: {
				Authorization: `Bearer ${accessToken}`,
			},
		});

		const result = await res.json();
		return result;
	}

	async updateSetting(username: string, hashedPassword: any, filename: any, user_id: number) {
		console.log("username", username);

		return camelCaseKeys(
			await this.knex.raw(
				/*sql*/
				`update users set username = COALESCE(?, username),
				password = COALESCE(?, password),
				avatar = COALESCE(?, avatar)
				where id = ?;`,
				[username, hashedPassword, filename, user_id]
			)
		);
	}

	async getQuestionStatusByUser(user_id: number, question_id: number) {
		return (
			await this.knex('user_completion_history')
				.select("user_id", "question_id", "complete_status", "created_at")
				.where({ user_id: user_id, question_id: question_id })
				.orderBy("created_at", "desc")
		)[0]
	}
}
