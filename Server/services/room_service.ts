import { Knex } from "knex";
import fetch from "node-fetch";

export class RoomService {
    constructor(private knex: Knex) { }

    // room_URL -> participants, created_by_user_id, avatar, question(all) where (room_id = ?)
    async getRoomInfo(room_URL: string) {
        return (
            await this.knex('code_room')
                .select([
                    "code_room.id as code_room_id",
                    "code_room.created_by_user_id",
                    // "users.id as user_id",
                    "code_room.status as code_room_status",
                    "room_id as room_URL",
                    "participant",
                    "room_participants.user_id",
                    "avatar",
                    "questions.id as questions_id",
                    "questions.question",
                    "questions.question_md",
                    "questions.validation_file",
                    "programming_language"
                ])
                .join('questions', 'code_room.question_id', '=', 'questions.id')
                .join('users', 'code_room.created_by_user_id', '=', 'users.id')
                .join('room_participants', 'code_room.id', '=', 'room_participants.code_room_id')
                .join('programming_languages', 'questions.programming_languages_id', '=', 'programming_languages.id')
                .where({ room_id: room_URL })
        )
    }

    async getRoomInfoByUserId(userId: number) {
        return (
            await this.knex('code_room')
                .select([
                    "code_room.id as code_room_id",
                    "code_room.created_by_user_id as code_room_created_by_user_id",
                    "questions.created_by_user_id as questions_created_by_user_id",
                    "code_room.status as code_room_status",
                    "room_id as room_URL",
                    "participant",
                    "room_participants.user_id",
                    "avatar",
                    "questions.question",
                    "questions.question_md",
                    "questions.validation_file",
                    "programming_language"
                ])
                .join('questions', 'code_room.question_id', '=', 'questions.id')
                .join('users', 'code_room.created_by_user_id', '=', 'users.id')
                .join('room_participants', 'code_room.id', '=', 'room_participants.code_room_id')
                .join('programming_languages', 'questions.programming_languages_id', '=', 'programming_languages.id')
                .where('code_room.created_by_user_id', '=', userId)
                .orderBy('code_room.created_at', 'desc')
        )
    }


    async createEmailDelivery(from: string, to: string, title: string, content: string, status: string) {
        return (
            await this.knex('email_delivery_history')
                .insert({ from, to, title, content, status })
                .returning(["id", "from", "to", "title", "content", "status"])
        )[0]
    }

    async createCodeRoom(room_id: string, created_by_user_id: number, question_id: number) {
        return (
            await this.knex('code_room')
                .insert({ room_id, created_by_user_id, question_id })
                .returning(["id"])
        )[0]
    }

    async checkParticipateUserId(email: string) {
        return (
            await this.knex('users')
                .select("*")
                .where({ email: email })
                .returning(["id"])
        )[0]
    }

    async createRoomParticipate(code_room_id: number, participant: string, user_id: number | null) {
        return (
            await this.knex('room_participants')
                .insert({ code_room_id, participant, user_id })
        )[0]
    }

    async codeRoomInvitation(host_user_id: number, code_room_id: number, delivery_history_id: number) {
        return (
            await this.knex('code_room_invitation')
                .insert({ host_user_id, code_room_id, delivery_history_id })
                .returning(["id"])
        )[0]
    }

    async addUser(username: string, password: string, email: string, avatar: string | null) {
        return (
            await this.knex("users")
                .insert({ username, password, email, avatar })
                .returning(["id", "username", "email", "avatar", "role", "status"])
        )[0];
    }

    async insertUserCompletionHistory(user_id: number, question_id: number, complete_status: string) {
        return (
            await this.knex("user_completion_history")
                .insert({ user_id, question_id, complete_status })
        )
    }
}
