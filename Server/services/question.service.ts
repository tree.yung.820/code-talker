import { Knex } from "knex";
import fetch from "node-fetch";

export class QuestionService {
    constructor(private knex: Knex) { }

    async getQuestionsFromDB() {
        return (
            await this.knex("questions")
                .select("questions.id", "title", "question",
                    "question_md", "programming_languages.programming_language",
                    "users.username", "difficulties.difficulty",
                    "categories.name as category")
                .join("programming_languages", "questions.programming_languages_id", "=", "programming_languages.id")
                .join("users", "questions.created_by_user_id", "=", "users.id")
                .join("difficulties", "questions.difficulties_id", "=", "difficulties.id")
                .join("categories", "questions.categories_id", "=", "categories.id")
        )
    }


    async getQuestionDetails(questionId: number, userId: number) {
        let result = await this.knex.raw(`
           select q.id,
                  q.title,
                  q.question,
                  programming_language,
                  username as created_by,
                  difficulty,
                  c.name   as category,
                  CASE
                      WHEN (select count(*) from user_favoured uf where question_id = ? and user_id = ?) = 0 THEN false
                      ELSE true
                      END  as is_fav,
                  ? as current_user

           from questions q
                    join programming_languages pl on pl.id = q.programming_languages_id
                    join users on q.created_by_user_id = users.id
                    join difficulties d on d.id = q.difficulties_id
                    join categories c on c.id = q.categories_id

           where q.id = ?
        `, [questionId, userId, userId, questionId])
        return result.rows[0]
    }



    async insertQuestionToDB(user_id: number, question_id: number) {
        return (
            await this.knex("user_favoured")
                .insert({ user_id, question_id })
                .returning(["user_id", "question_id"])
        )[0]
    }

    async deleteQuestionToDB(user_id: number, question_id: number) {
        return (
            await this.knex("user_favoured")
                .where({ user_id: user_id, question_id: question_id })
                .del()
        )
    }

    async getGeneralPopularQuestion() {
        return (
            await this.knex('user_completion_history')
                .select('question_id', 'title', 'difficulty')
                .count('user_id')
                .groupBy('question_id', 'questions.title', 'difficulties.difficulty')
                .join('questions', 'user_completion_history.question_id', '=', 'questions.id')
                .join("difficulties", "questions.difficulties_id", "=", "difficulties.id")
                .orderBy('count', 'desc')
        )[0]
    }

    async getEasyPopularQuestion() {
        return (
            await this.knex('user_completion_history')
                .select('question_id', 'title', 'difficulty')
                .count('user_id')
                .groupBy('question_id', 'questions.title', 'difficulties.difficulty')
                .join('questions', 'user_completion_history.question_id', '=', 'questions.id')
                .join("difficulties", "questions.difficulties_id", "=", "difficulties.id")
                .orderBy('count', 'desc')
                .where({ difficulty: 'Easy' })
        )[0]
    }

    async getMediumPopularQuestion() {
        return (
            await this.knex('user_completion_history')
                .select('question_id', 'title', 'difficulty')
                .count('user_id')
                .groupBy('question_id', 'questions.title', 'difficulties.difficulty')
                .join('questions', 'user_completion_history.question_id', '=', 'questions.id')
                .join("difficulties", "questions.difficulties_id", "=", "difficulties.id")
                .orderBy('count', 'desc')
                .where({ difficulty: 'Medium' })
        )[0]
    }

    async getHardPopularQuestion() {
        return (
            await this.knex('user_completion_history')
                .select('question_id', 'title', 'difficulty')
                .count('user_id')
                .groupBy('question_id', 'questions.title', 'difficulties.difficulty')
                .join('questions', 'user_completion_history.question_id', '=', 'questions.id')
                .join("difficulties", "questions.difficulties_id", "=", "difficulties.id")
                .orderBy('count', 'desc')
                .where({ difficulty: 'Hard' })
        )[0]
    }
}
